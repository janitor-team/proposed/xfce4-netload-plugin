# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Elishai Eliyahu <e1907@mm.st>, 2017
# GenghisKhan <genghiskhan@gmx.ca>, 2015
# Hebrew speakers the world over, 2013
# Yaron Shahrabani <sh.yaron@gmail.com>, 2013
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-06-09 12:31+0200\n"
"PO-Revision-Date: 2017-09-23 19:03+0000\n"
"Last-Translator: Elishai Eliyahu <e1907@mm.st>\n"
"Language-Team: Hebrew (http://www.transifex.com/xfce/xfce-panel-plugins/language/he/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: he\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % 1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;\n"

#. Defaults
#: ../panel-plugin/netload.c:42
msgid "Net"
msgstr "רשת"

#: ../panel-plugin/netload.c:61
msgid "Xfce4-Netload-Plugin"
msgstr "Xfce4-Netload-Plugin"

#: ../panel-plugin/netload.c:64
msgid "Unknown error."
msgstr "שגיאה לא מוכרת."

#: ../panel-plugin/netload.c:65
msgid "Linux proc device '/proc/net/dev' not found."
msgstr "התקן Linux proc ‏'‎/proc/net/dev' לא נמצא."

#: ../panel-plugin/netload.c:66
msgid "Interface was not found."
msgstr "ממשק לא נמצא."

#: ../panel-plugin/netload.c:168
#, c-format
msgid "<< %s >> (Interface down)"
msgstr "‏<< %s >> (ממשק מושבת)"

#: ../panel-plugin/netload.c:255
#, c-format
msgid ""
"<< %s >> (%s)\n"
"Average of last %d measures\n"
"with an interval of %.2fs:\n"
"Incoming: %s\n"
"Outgoing: %s\n"
"Total: %s"
msgstr "‏<< %s >> ‏(%s)\nממוצע של %d מדידות אחרונות\nבתדירות של %.2f ש׳:\nנכנסת: %s\nיוצאת: %s\nסך הכול: %s"

#: ../panel-plugin/netload.c:258
msgid "no IP address"
msgstr "אין כתובת ‏IP"

#: ../panel-plugin/netload.c:642
#, c-format
msgid ""
"%s: Error in initializing:\n"
"%s"
msgstr "‏%s: שגיאה באתחול:\n%s"

#: ../panel-plugin/netload.c:980
msgid "Bar color (i_ncoming):"
msgstr "צבע עמודה (נ_כנסת):"

#: ../panel-plugin/netload.c:981
msgid "Bar color (_outgoing):"
msgstr "צבע עמודה (יו_צאת):"

#: ../panel-plugin/netload.c:984
msgid "Maximum (inco_ming):"
msgstr "מרבי (_נכנסת):"

#: ../panel-plugin/netload.c:985
msgid "Maximum (o_utgoing):"
msgstr "מרבי (_יוצאת):"

#: ../panel-plugin/netload.c:990 ../panel-plugin/netload.desktop.in.in.h:1
msgid "Network Monitor"
msgstr "צג רשת"

#: ../panel-plugin/netload.c:1019
msgid "_Text to display:"
msgstr "_טקסט להצגה:"

#: ../panel-plugin/netload.c:1050
msgid "Network _device:"
msgstr "התקן _רשת:"

#: ../panel-plugin/netload.c:1077
msgid "Update _interval:"
msgstr "ת_דירות עדכון:"

#: ../panel-plugin/netload.c:1090
msgid "s"
msgstr "ש׳"

#: ../panel-plugin/netload.c:1104
msgid "Show values as _bits"
msgstr "הצג ערכים בתור סי_בית"

#: ../panel-plugin/netload.c:1118
msgid "_Automatic maximum"
msgstr "ערך מרבי _אוטומטי"

#: ../panel-plugin/netload.c:1152
msgid "KiB/s"
msgstr "KiB/s"

#: ../panel-plugin/netload.c:1177
msgid "_Present data as:"
msgstr "ה_צג נתונים בתור:"

#: ../panel-plugin/netload.c:1186
msgid "Bars"
msgstr "שורות"

#: ../panel-plugin/netload.c:1187
msgid "Values"
msgstr "ערכים"

#: ../panel-plugin/netload.c:1188
msgid "Bars and values"
msgstr "שורות וערכים"

#: ../panel-plugin/netload.c:1230
msgid "_Colorize values"
msgstr "צ_בע ערכים"

#: ../panel-plugin/netload.desktop.in.in.h:2
msgid "Show network traffic"
msgstr "הצג תעבורת רשת"

#: ../panel-plugin/utils.c:78
msgid "B"
msgstr "ב׳"

#: ../panel-plugin/utils.c:78
msgid "KiB"
msgstr "KiB"

#: ../panel-plugin/utils.c:78
msgid "MiB"
msgstr "MiB"

#: ../panel-plugin/utils.c:78
msgid "GiB"
msgstr "GiB"

#: ../panel-plugin/utils.c:79
msgid "bps"
msgstr "bps"

#: ../panel-plugin/utils.c:79
msgid "Kbps"
msgstr "Kbps"

#: ../panel-plugin/utils.c:79
msgid "Mbps"
msgstr "Mbps"

#: ../panel-plugin/utils.c:79
msgid "Gbps"
msgstr "Gbps"
